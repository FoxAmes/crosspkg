use libpowerpelet::PkgBuild;

use self::common::CommonPatches;

pub mod common;

#[derive(Debug, Clone)]
pub enum PatchFailure {
    NotApplicable,
    NotUtf,
    IOError,
}

pub type PatchResult = Result<(), PatchFailure>;

pub fn patch_pkbguild(
    pkb: &mut PkgBuild,
    ps: &Vec<CommonPatches>,
    arch: &str,
    host: &str,
) -> Result<(), String> {
    // Apply common patches
    let mut ps = ps.clone();
    ps.dedup_by(|a, b| *a == *b);
    for p in &ps {
        match p.patch(pkb, arch, host) {
            Ok(_) => {}
            Err(e) => match e {
                PatchFailure::NotApplicable => {}
                _ => return Err(format!("Unable applying patch {:?}: {:?}", p, e)),
            },
        }
    }
    Ok(())
}
