use libpowerpelet::PkgBuild;

use {
    super::PatchResult,
    regex::{Regex, Replacer},
    std::{fs::File, io::Write},
};

#[derive(clap::ValueEnum, Clone, Debug, PartialEq, Eq)]
/// Common patches useful for a wide variety of packages
pub enum CommonPatches {
    /// Patches the `arch` field of the PKGBUILD
    Arch,
    /// Patches `gcc` binary calls to instead use host-tuple-gcc
    Gcc,
    /// Patches `configure` script calls, adding `--host`
    Configure,
    /// Patches `meson`, adding a generated `--cross-file`
    Meson,
}

impl CommonPatches {
    /// Apply a common patch.
    pub fn patch(&self, pkb: &mut PkgBuild, arch: &str, host: &str) -> PatchResult {
        match self {
            CommonPatches::Arch => match pkb
                .arch
                .iter()
                .map(|a| a.trim_matches('\'').trim_matches('"'))
                .collect::<Vec<&str>>()
                .contains(&"any")
            {
                true => return Ok(()),
                false => {
                    let a = format!("'{}'", arch);
                    if !pkb.arch.contains(&a) {
                        pkb.arch.push(a);
                    }
                    return Ok(());
                }
            },
            CommonPatches::Gcc => {
                let reg = Regex::new(r"([^-])gcc ").unwrap();
                for func in vec![
                    Some(&mut pkb.package),
                    pkb.prepare.as_mut(),
                    pkb.build.as_mut(),
                    pkb.check.as_mut(),
                ] {
                    if let Some(func) = func {
                        patch_regex(func, &reg, format!("\\1{}-gcc ", host), None);
                    }
                }
            }
            CommonPatches::Configure => {
                let reg = Regex::new(r"configure ").unwrap();
                let rep = format!("configure --host={} ", host);
                for func in vec![
                    Some(&mut pkb.package),
                    pkb.prepare.as_mut(),
                    pkb.build.as_mut(),
                    pkb.check.as_mut(),
                ] {
                    if let Some(func) = func {
                        patch_regex(func, &reg, &rep, Some(&Regex::new(&rep).unwrap()))
                    }
                }
            }
            CommonPatches::Meson => {
                gen_meson_cross_file(host, arch);
                for func in vec![
                    Some(&mut pkb.package),
                    pkb.prepare.as_mut(),
                    pkb.build.as_mut(),
                    pkb.check.as_mut(),
                ] {
                    if let Some(func) = func {
                        let reg = Regex::new(r"meson setup ").unwrap();
                        let rep = format!("meson setup --cross-file={}.txt ", host);
                        patch_regex(func, &reg, &rep, Some(&Regex::new(&rep).unwrap()));
                        let reg = Regex::new(r"arch-meson ").unwrap();
                        let rep = format!("arch-meson --cross-file={}.txt ", host);
                        patch_regex(func, &reg, &rep, Some(&Regex::new(&rep).unwrap()));
                    }
                }
            }
        }
        pkb.save();
        Ok(())
    }
}

/// Generate a meson compatible cross-file.
pub fn gen_meson_cross_file(host: &str, arch: &str) {
    let mut f = File::create(host.to_string() + ".txt").expect("Error creating meson cross file");
    let cross_file_c = String::from(
        r"[binaries]
c = '",
    ) + host
        + r"-gcc'
cpp = '" + host
        + r"-g++'
ar = '" + host
        + "-ar'
strip = '"
        + host
        + "-strip'
exe_wrapper = 'qemu'

[host_machine]
system = 'linux'
cpu_family = '"
        + arch
        + "'
cpu = '" + arch
        + "'
endian = 'little'
";
    f.write_all(cross_file_c.as_bytes()).unwrap();
}

/// Patch a given file with a specified regex and replacer.
/// Optionally ignores matches that match a negative expression.
///
/// Requires `file` be opened both readable and writable.
/// Contents will be replaced.
fn patch_regex<T>(text: &mut String, reg: &Regex, mut rep: T, negative: Option<&Regex>)
where
    T: Replacer,
{
    // Replace regex
    match negative {
        Some(negative) => {
            let mut pos = 0;
            while let Some(m) = reg.find_at(&text.clone(), pos) {
                pos = m.end();
                if !negative.is_match_at(text, m.start()) {
                    text.replace_range(m.range(), &reg.replace(m.as_str(), rep.by_ref()));
                }
            }
        }
        None => *text = reg.replace_all(text, rep).to_string(),
    };
}
