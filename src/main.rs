mod patches;

use std::collections::HashMap;
use std::io::Write;
use std::path::{Path, PathBuf};
use std::{fs::File, process::exit};

use alpm::{Depend, Package, SigLevel};
use clap::Parser;
use git2::Repository;
use libpowerpelet::PkgBuild;
use log::{debug, error, info, trace, warn};
use patches::common::CommonPatches;
use simplelog::{
    ColorChoice, CombinedLogger, Config, LevelFilter, SharedLogger, TermLogger, TerminalMode,
    WriteLogger,
};

use crate::patches::patch_pkbguild;

#[derive(Parser, Debug)]
/// Container for `clap` args
pub struct CLArgs {
    /// Architecture of the output package
    #[arg(short, long)]
    arch: String,
    /// Host tuple for the toolchain
    #[arg(long)]
    host: String,

    /// Arguments to pass to makepkg
    #[arg(long, default_values_t = vec![String::from("-srf"), String::from("--noconfirm")])]
    makepkg_args: Vec<String>,

    /// Which common PKGBUILD patches to apply
    #[arg(
        short,
        long,
        value_enum,
        value_delimiter = Some(','),
        num_args = 0..,
        default_values_t = vec![
            CommonPatches::Arch,
            CommonPatches::Gcc,
            CommonPatches::Configure,
            CommonPatches::Meson
        ]
    )]
    common_patches: Vec<CommonPatches>,

    /// Package to build
    #[arg(short, long)]
    package: String,

    /// Directory to work in.  With `package`, path to clone to
    #[arg(short, long, default_value_t = String::from("./"))]
    directory: String,

    /// Fetch PGP keys before building
    #[arg(long, default_value_t = false)]
    fetch_pgp: bool,

    /// Build dependencies, if present
    #[arg(long, default_value_t = true)]
    build_deps: bool,

    /// Verbosity to log with
    #[arg(long, default_value_t = LevelFilter::Warn)]
    log_level: LevelFilter,

    /// Log to the specified file
    #[arg(long, default_value = None)]
    log_file: Option<PathBuf>,

    /// Verbosity to log to file
    #[arg(long, default_value_t = LevelFilter::Info)]
    log_file_level: LevelFilter,
}

/// Clone a package
fn clone_pkg(pkg: &str, path: &Path) -> Result<(), String> {
    // Checkout package branch from arch svntogit repo
    info!("Cloning package {}...", pkg);
    // Initialize a repository
    let repo = match Repository::init(&path) {
        Err(e) => return Err(format!("Failed to create repo: {}", e.message())),
        Ok(r) => r,
    };
    // Set remote
    let mut remote = match repo.find_remote("origin") {
        Ok(r) => r,
        Err(_) => match repo.remote("origin", "https://github.com/archlinux/svntogit-packages") {
            Err(e) => return Err(format!("Unable to set git remote: {}", e.message())),
            Ok(r) => r,
        },
    };
    // Connect to remote
    match remote.connect(git2::Direction::Fetch) {
        Err(e) => {
            return Err(format!(
                "Unable to connect to remote {}: {}",
                remote.url().unwrap(),
                e.message()
            ))
        }
        Ok(_) => {}
    }
    // Fetch from remote
    match remote.fetch(&["packages/".to_string() + &pkg], None, None) {
        Err(e) => return Err(format!("Unable to fetch: {}", e.message())),
        Ok(_) => {}
    }
    // Disconnect from remote
    match remote.disconnect() {
        Err(e) => return Err(format!("Unable to disconnect: {}", e.message())),
        Ok(_) => {}
    }
    // Set head to package branch
    match repo.set_head(&("refs/remotes/origin/packages/".to_string() + &pkg)) {
        Err(e) => {
            return Err(format!(
                "Unable to update head to package branch: {}",
                e.message()
            ))
        }
        Ok(_) => {}
    }
    // Checkout head
    match repo.checkout_head(None) {
        Err(e) => {
            return Err(format!(
                "Unable to checkout package branch: {}",
                e.message()
            ))
        }
        Ok(_) => {}
    };
    info!("Cloned.");
    Ok(())
}

/// Fetch PGP keys
fn fetch_pgp(keys: &Vec<String>) -> Result<(), String> {
    if keys.len() > 0 {
        let mut keys = keys.clone();
        for k in &mut keys {
            for c in vec!['\'', '"'] {
                *k = k.trim_matches(c).to_string();
            }
        }
        let mut args = vec!["--recv-keys".to_string()];
        args.extend(keys);
        let mut _gpg = match std::process::Command::new("gpg")
            .args(args)
            .stderr(std::process::Stdio::piped())
            .stdout(std::process::Stdio::piped())
            .spawn()
        {
            Ok(g) => g,
            Err(e) => return Err(format!("Error spawning gpg: {}", e)),
        };
        match _gpg.wait_with_output() {
            Err(e) => return Err(format!("Error waiting for gpg: {}", e)),
            Ok(o) => match o.status.success() {
                true => {}
                false => {
                    return Err(format!(
                        "Error running gpg: {}",
                        std::str::from_utf8(&o.stderr).unwrap()
                    ))
                }
            },
        }
    }
    Ok(())
}

/// Get a list of dependencies for a given package
fn resolve_deps<'a>(
    pkg: &str,
    nested: bool,
    handle: &'a alpm::Alpm,
    resolved: Option<&mut Vec<Depend>>,
) -> Vec<(Depend, Package<'a>)> {
    let mut deps = Vec::<(Depend, Package)>::new();

    // If given an existing list of resolved dependencies, use it
    // Otherwise, operate on a new list
    let mut t = Vec::new();
    let resolved = match resolved {
        Some(r) => r,
        None => &mut t,
    };

    // Iterate over databases
    let dbs = handle.syncdbs();
    trace!("Searching for {}...", pkg);
    for db in dbs {
        // Find package matching query
        match db.pkg(pkg) {
            Ok(pkg) => {
                // Iterate over package dependencies
                for dep in pkg.depends() {
                    // If we've already resolved, skip
                    if resolved.contains(&dep.to_depend()) {
                        continue;
                    }
                    // Find dependency within known dbs
                    match dbs.find_satisfier(dep.dep()) {
                        // Found satisfier
                        Some(sat) => {
                            resolved.push(dep.to_depend());
                            // Append satisfier to list
                            deps.push((dep.to_depend(), sat));
                            // Resolve nested dependencies
                            if nested {
                                // Recursive call to resolve
                                deps.extend(resolve_deps(
                                    sat.name(),
                                    nested,
                                    handle,
                                    Some(resolved),
                                ));
                            }
                        }
                        // Warn on missing dependency
                        None => {
                            warn!(
                                "Unable to find satifier for {}, required by {}",
                                dep.name(),
                                pkg.name()
                            )
                        }
                    }
                }
                // We've found satisfier, no need to continue
                break;
            }
            Err(e) => error!("Error finding {} in {}: {}", pkg, db.name(), e),
        }
        trace!("No matching package in {} for {}.", db.name(), pkg);
    }
    // Return found dependencies
    deps
}

/// Build package
fn build_pkg(pkg: &str, args: &CLArgs, path: &Path) -> Result<(), String> {
    let basepath = path.canonicalize().unwrap();
    let mut path = path.join(pkg);

    // Clone
    match clone_pkg(pkg, &path) {
        Err(e) => return Err(format!("Unable to clone {}: {}", pkg, e)),
        Ok(_) => {}
    }

    // Use `trunk` folder
    path = path.join("trunk");
    debug!("Set path to {:?}.", path);

    // Open the pkgbuild
    let mut pkb = match PkgBuild::open(path.join("PKGBUILD")) {
        Ok(p) => p,
        Err(_) => return Err(format!("Unable to parse PKGBUILD.")),
    };

    // Apply patches
    match patch_pkbguild(&mut pkb, &args.common_patches, &args.arch, &args.host) {
        Err(e) => return Err(format!("{}", e)),
        Ok(_) => {}
    }

    // Save pkgbuild
    match pkb.save() {
        Err(e) => return Err(format!("Unable to save PKGBUILD: {}", e)),
        Ok(_) => {}
    }

    // Fetch GPG keys if requested
    if args.fetch_pgp {
        if let Some(keys) = pkb.validpgpkeys {
            info!("Fetching PGP keys...");
            match fetch_pgp(&keys) {
                Ok(_) => {}
                Err(e) => return Err(format!("Error fetching PGP keys: {}", e)),
            }
        }
    }

    // Spawn makepkg
    info!("Running makepkg...");
    let mut makepkg_args = vec![
        "--config".to_string(),
        basepath
            .join(".makepkg")
            .join("makepkg.conf")
            .to_string_lossy()
            .to_string(),
    ];
    makepkg_args.extend(args.makepkg_args.clone());
    let mut _child = match std::process::Command::new("makepkg")
        .current_dir(&path)
        .args(&makepkg_args)
        .stderr(std::process::Stdio::piped())
        .stdout(std::process::Stdio::piped())
        .spawn()
    {
        Ok(p) => p,
        Err(e) => return Err(format!("Error spawning makepkg: {}", e)),
    };

    // Await makepkg
    match _child.wait_with_output() {
        Ok(o) => match o.status.success() {
            true => {}
            false => {
                return Err(format!(
                    "Error running makepkg: {}",
                    std::str::from_utf8(&o.stderr).unwrap()
                ))
            }
        },
        Err(e) => return Err(format!("Error waiting for makepkg: {}", e)),
    };
    Ok(())
}

/// Initialize databases in ALPM
fn init_syncdbs(handle: &mut alpm::Alpm) -> Result<(), String> {
    // Core DBs to sync
    let rdbs = vec!["core", "extra", "community"];
    // Iterate over exiting DBs
    let sdbs = handle
        .syncdbs()
        .iter()
        .map(|db| db.name().to_string())
        .collect::<Vec<String>>();
    // Ensure our desired DBs are present
    for rdb in rdbs {
        // If DB does not already exist, register
        if !sdbs.contains(&rdb.to_string()) {
            trace!("Registering {}...", rdb);
            let db = match handle.register_syncdb_mut(rdb, SigLevel::USE_DEFAULT) {
                Ok(db) => db,
                Err(e) => return Err(format!("Error registering db {}: {}", rdb, e)),
            };
            let url = format!("https://mirror.arizona.edu/archlinux/{}/os/x86_64", rdb);
            info!("Adding server: {}", url);
            if let Err(e) = db.add_server(url) {
                return Err(format!("Error adding server for db {}: {}", rdb, e));
            }
        }
    }
    // Synchronize the DBs
    info!("Syncing DBs...");
    if let Err(e) = handle.syncdbs_mut().update(true) {
        return Err(format!("Error syncing DBs during initialization: {}", e));
    }
    info!("Done syncing DBs.");
    Ok(())
}

/// Generate a makepkg configuration file
fn gen_makepkg_conf(arch: &str, host: &str, basedir: &Path) -> Result<(), String> {
    let makepkg_dir = PathBuf::from("packages").join(".makepkg");
    if let Err(e) = std::fs::create_dir_all(&makepkg_dir) {
        return Err(format!("Error creating .makepkg directory: {}", e));
    }
    let mut file = match File::create(makepkg_dir.join("makepkg.conf")) {
        Err(e) => return Err(format!("Error creating makepkg.conf: {}", e)),
        Ok(f) => f,
    };
    let content = format!("#!/hint/bash
DLAGENTS=('file::/usr/bin/curl -qgC - -o %o %u'
    'ftp::/usr/bin/curl -qgfC - --ftp-pasv --retry 3 --retry-delay 3 -o %o %u'
    'http::/usr/bin/curl -qgb \"\" -fLC - --retry 3 --retry-delay 3 -o %o %u'
    'https::/usr/bin/curl -qgb \"\" -fLC - --retry 3 --retry-delay 3 -o %o %u'
    'rsync::/usr/bin/rsync --no-motd -z %u %o'
    'scp::/usr/bin/scp -C %u %o')
VCSCLIENTS=('bzr::bzr'
    'fossil::fossil'
    'git::git'
    'hg::mercurial'
    'svn::subversion')
CC=\"{1}-gcc\"
CROSS_COMPILE=\"{1}-\"
CARCH=\"{0}\"
CHOST=\"{1}\"
CFLAGS=\"-O2 -mno-plt -pipe -fexceptions -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security -fstack-clash-protection\"
CXXFLAGS=\"$CFLAGS -Wp,-D_GLIBCXX_ASSERTIONS\"
LDFLAGS=\"-Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now\"
LTOFLAGS=\"-flto=auto\"
MAKEFLAGS=\"CC=$CC CROSS_COMPILE=$CROSS_COMPILE CHOST=$CHOST -j$(($(nproc)+1))\"
DEBUG_CFLAGS=\"-g\"
DEBUG_CXXFLAGS=\"$DEBUG_CFLAGS\"
BUILDENV=(!distcc color !ccache !check !sign)
BUILDDIR={2}/.tmp
OPTIONS=(!strip docs !libtool !staticlibs emptydirs zipman purge !debug !lto)
INTEGRITY_CHECK=(sha256)
STRIP_BINARIES=\"--strip-all\"
STRIP_SHARED=\"--strip-unneeded\"
STRIP_STATIC=\"--strip-debug\"
MAN_DIRS=({{usr{{,/local}}{{,/share}},opt/*}}/{{man,info}})
DOC_DIRS=(usr/{{,local/}}{{,share/}}{{doc,gtk-doc}} opt/*/{{doc,gtk-doc}})
PURGE_TARGETS=(usr/{{,share}}/info/dir .packlist *.pod)
DBGSRCDIR=\"/usr/src/debug\"
PKGDEST={2}/.build
SRCDEST={2}/.makepkg/sources
SRCPKGDEST={2}/.makepkg/srcpackages
LOGDEST={2}/.makepkg/logs
COMPRESSGZ=(gzip -c -f -n)
COMPRESSBZ2=(bzip2 -c -f)
COMPRESSXZ=(xz -c -z -)
COMPRESSZST=(zstd -c -z -q -)
COMPRESSLRZ=(lrzip -q)
COMPRESSLZO=(lzop -q)
COMPRESSZ=(compress -c -f)
COMPRESSLZ4=(lz4 -q)
COMPRESSLZ=(lzip -c -f)
PKGEXT='.pkg.tar.zst'
SRCEXT='.src.tar.gz'", arch, host, basedir.canonicalize().unwrap().to_string_lossy());
    if let Err(e) = file.write_all(content.as_bytes()) {
        return Err(format!("Error writing makepkg.conf: {}", e));
    }
    Ok(())
}

fn main() {
    // Parse arguments
    let args = CLArgs::parse();

    // Init logger
    let mut loggers: Vec<Box<dyn SharedLogger>> = vec![TermLogger::new(
        args.log_level,
        Config::default(),
        TerminalMode::Mixed,
        ColorChoice::Auto,
    )];
    if let Some(l) = &args.log_file {
        loggers.push(WriteLogger::new(
            args.log_file_level,
            Config::default(),
            File::create(l).unwrap(),
        ));
    }
    CombinedLogger::init(loggers).unwrap();

    // Validate directory
    let d_path = std::path::PathBuf::from(&args.directory);
    if !d_path.is_dir() {
        if !d_path.exists() {
            match std::fs::create_dir_all(&d_path) {
                Err(e) => {
                    error!(
                        "Unable to create {}: {}",
                        d_path.to_string_lossy(),
                        e.to_string()
                    );
                    exit(1);
                }
                Ok(_) => {}
            }
        } else {
            error!(
                "{} is not a directory, exiting",
                d_path.as_os_str().to_string_lossy()
            );
            exit(1);
        }
    }

    let mut pkgs = HashMap::<String, bool>::new();
    pkgs.insert(args.package.clone(), false);
    // Resolve dependencies
    if args.build_deps {
        // Initialize temporary pacman DBs
        let alpm_dir = d_path.join(".alpm");
        if !alpm_dir.exists() {
            std::fs::create_dir(&alpm_dir);
        }
        let db_dir = alpm_dir.join("db");
        if !db_dir.exists() {
            std::fs::create_dir(&db_dir);
        }
        match alpm::Alpm::new(
            alpm_dir.to_string_lossy().to_string(),
            db_dir.to_string_lossy().to_string(),
        ) {
            Ok(mut handle) => {
                if let Err(e) = init_syncdbs(&mut handle) {
                    error!("Error initializing syncdbs: {}", e);
                    exit(2);
                }
                for (_dep, pkg) in resolve_deps(&args.package, true, &handle, None) {
                    pkgs.insert(pkg.name().to_string(), false);
                }
            }
            Err(e) => {
                error!("Unable to open ALPM database: {}", e.to_string());
                exit(2);
            }
        }
    }

    // Print status
    info!("To build: {} packages", pkgs.len());
    trace!(
        "\t{}",
        pkgs.keys()
            .map(|k| k.as_str())
            .collect::<Vec<&str>>()
            .join(" ")
    );

    // Generate makepkg config
    if let Err(e) = gen_makepkg_conf(&args.arch, &args.host, &d_path) {
        error!("Unable to generate makepkg.conf: {}", e);
        exit(3);
    }

    // Build package
    for (pkg, val) in &mut pkgs {
        info!("Building {}...", &pkg);
        match build_pkg(&pkg, &args, &d_path) {
            Ok(_) => {
                *val = true;
                info!("Finished building {}.", &pkg)
            }
            Err(e) => {
                error!("Error building {}: {}", &pkg, &e)
            }
        }
    }

    // Print status
    let mut successful = Vec::<&str>::new();
    let mut failed = Vec::<&str>::new();
    for (k, v) in &pkgs {
        if *v {
            successful.push(k.as_str());
        } else {
            failed.push(k.as_str());
        }
    }
    info!(
        "Built {}/{} packages: {}",
        successful.len(),
        pkgs.len(),
        successful.join(" ")
    );
    if failed.len() > 0 {
        warn!(
            "Failed to build {}/{} packages: {}",
            failed.len(),
            pkgs.len(),
            failed.join(" ")
        );
    }
}
